"screens/vgui_test_screen.res"
{
    "Background"
    {
        "ControlName"   "MaterialImage"
        "fieldName"     "Background"
        "xpos"          "0"
        "ypos"          "0"
        "zpos"          "-2"
        "wide"          "480"
        "tall"          "240"

        "material"      "launcher/background"
    }

    "OwnerReadout"
    {
        "ControlName"   "Label"
        "fieldName"     "OwnerReadout"
        "xpos"          "10"
        "ypos"          "20"
        "wide"          "240"
        "tall"          "34"
        "autoResize"    "0"
        "pinCorner"     "0"
        "visible"       "1"
        "enabled"       "1"
        "tabPosition"   "0"
        "labelText"     "No Owner"
        "textAlignment" "center"
        "dulltext"      "0"
        "paintBackground" "0"
    }

    "HealthReadout"
    {
        "ControlName"   "Label"
        "fieldName"     "HealthReadout"
        "xpos"          "200"
        "ypos"          "20"
        "wide"          "240"
        "tall"          "34"
        "autoResize"    "0"
        "pinCorner"     "0"
        "visible"       "1"
        "enabled"       "1"
        "tabPosition"   "0"
        "labelText"     "Health: 100%"
        "textAlignment" "center"
        "dulltext"      "0"
        "paintBackground" "0"
    }

    "DismantleButton"
    {
        "ControlName"   "MaterialButton"
        "fieldName"     "Dismantle"
		"visible"		"1"
		"enabled"		"1"
        "labelText"     "Dismantle"
    }
	
	"MountButton"
    {
        "ControlName"   "Button"
        "fieldName"     "MountButton"
		"visible"		"1"
		"enabled"		"1"
		"xpos"		"200"
		"ypos"		"0"
		"wide"		"64"
		"tall"		"24"
        "labelText"     "Mount"
		"Command"	"QuitGame"
    }
	
	"EditField"
	{
		"ControlName"		"TextEntry"
		"fieldName"		"EditField"
		"xpos"		"40"
		"ypos"		"60"
		"wide"		"300"
		"tall"		"20"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"1"
		"textHidden"		"0"
		"editable"		"1"
		"maxchars"		"32"
		"default"		"1"
	}

	"HTML"
	{
		"ControlName"		"HTML"
		"fieldName"		"HTMLTest"
		"xpos"		"40"
		"ypos"		"80"
		"wide"		"300"
		"tall"		"150"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
		"OpenURL"		"html.html"
		"URL"		"html.html"
	}
}